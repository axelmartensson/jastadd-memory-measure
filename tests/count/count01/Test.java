// Tests that a synthesized NTA can not be changed to uncached.
// .grammar: { A; }
// .result=OUTPUT_PASS
import static runtime.Test.*;
public class Test {
  public static void main(String[] args) {
    A a = new A();
    MemAnalysis.objectSize(a);
  }
}
