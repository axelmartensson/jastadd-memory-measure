// Tests that a synthesized NTA can not be changed to uncached.
// .grammar: { A ::= B C; B; C ::= D; D; }
// .result=OUTPUT_PASS
import static runtime.Test.*;
public class Test {
  public static void main(String[] args) {
    A a = new A(null, new C(new D()));
    MemAnalysis.objectSize(a);
  }
}
