package measure;

import drast.model.ASTAnnotation;
import org.openjdk.jol.datamodel.X86_64_DataModel;
import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.info.FieldLayout;
import org.openjdk.jol.vm.VM;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by axel on 3/5/17.
 */
public class Overhead extends MemAnalysis {
  private static String[] astReprFields = new String[]{"ASTNode.parent", "ASTNode.children", "ASTNode.childIndex", "ASTNode.numChildren"};
  private long astrepresentationsize;
  private long cachefieldsize;
  private long otherfieldsize;
  private long cachestructuresize;
  private X86_64_DataModel dm;

  private final int CUTOFF = 50;

  public Overhead() throws FileNotFoundException {
    super("overhead.csv", "astrepresentationsize cachefieldsize otherfieldsize cachestructuresize testcase");
    this.dm = new X86_64_DataModel();
  }

  @Override
  public void analyzeNode(Object node, ASTClass astClass) {

    try {

      ClassLayout classLayout = ClassLayout.parseClass(node.getClass());
      //TODO:  will not change for given run, compute once for one node and reuse
     astrepresentationsize += classLayout.headerSize(); // object header

      for (String fieldname : astReprFields) {
        try {
          long cost = fieldCost(classLayout, fieldname);
          astrepresentationsize += cost;
        } catch (NoSuchFieldException e){
            throw new RuntimeException("field ASTNode."+fieldname+" not found in instance of "+astClass.getName());
        }
      }

     Object children = findField(node, "children").get(node);
     astrepresentationsize += children==null?0:VM.current().sizeOf(children); // actual children array size

       for (FieldLayout fieldLayout :classLayout.fields()) {
           if ( belongsToAttributeCache(fieldLayout.name())) {
             cachefieldsize += fieldLayout.size();
           } else if ( belongsToAttributeOther(fieldLayout.name())) {
             otherfieldsize += fieldLayout.size();
           }
     }

    for (Method method : astClass.getParameterized()) {
      Field f = findField(node, ASTAnnotation.getCacheValueFieldName(method));
      if (f != null) {
        Object cachedValue = null;
        try {
          cachedValue = f.get(node);
        } catch (IllegalAccessException e) {
          e.printStackTrace();
        }
        if (cachedValue != null) {
          try {
            Map cache = (Map) cachedValue;
            Class cacheklass = cache.getClass();
            //todo stryk cachestructuresize och förlita på output från paramcacheelementcount
            if (cacheklass.getSimpleName().equals("HashMap")) {
                cachestructuresize += analyzeHashMap(cache, cacheklass);
            } else {
              throw new RuntimeException("Unknown cache type: " + cacheklass.getSimpleName());
            }
            Field computedfield = findField(node, ASTAnnotation.getComputedFlagFieldName(method));
            if (computedfield != null) {
              Object computed = computedfield.get(node);
              if (computed != null) {
                Class computedklass = computed.getClass();
                if (computedklass.getSimpleName().equals("HashMap")) {
                  cachestructuresize += analyzeHashMap((Map) computed, computedklass);
                } else {
                  throw new RuntimeException("Unknown cache type: " + cacheklass.getSimpleName());
                }
              }
            }
          } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
          }
        }
      }
    }
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  private long fieldCost(ClassLayout classLayout, String name) throws NoSuchFieldException{
    for (FieldLayout fieldLayout :classLayout.fields()) {
      if(fieldLayout.hostClass().equals("ASTNode") && fieldLayout.name().equals(name)) return fieldLayout.size();
    }
    throw new NoSuchFieldException("Field " + name + " not found in a node");
  }
  @Override
  protected void output(PrintWriter pw) {
    MemAnalysis.csvoutput(pw,
            astrepresentationsize,
            cachefieldsize,
            otherfieldsize,
            cachestructuresize);
  }

}
