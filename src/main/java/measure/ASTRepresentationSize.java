package measure;

import drast.model.ASTAnnotation;
import org.openjdk.jol.datamodel.X86_64_DataModel;
import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.info.FieldLayout;
import org.openjdk.jol.vm.VM;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by axel on 3/5/17.
 */
public class ASTRepresentationSize extends MemAnalysis {
  private static String[] astReprFields;
  private long astrepresentationsize;
  private final int CUTOFF = 50;

  public ASTRepresentationSize() throws FileNotFoundException {
    super("astrepresentationsize.csv", "astrepresentationsize testcase");
    String fl = System.getProperty("testcase").substring(0,1);
    boolean inModelica = fl.equals(fl.toUpperCase());
    if(inModelica) {
      astReprFields = new String[]{
              "Symbol","start",
              "Symbol","end",
              "Symbol","id",
              "Symbol","value",
              "ASTNode","childIndex",
              "ASTNode","numChildren",
              "ASTNode","in$Circle",
              "ASTNode","is$Final",
              "ASTNode","parent",
              "ASTNode","children"
      };

    } else {
      astReprFields = new String[]{
              "Symbol","start",
              "Symbol","end",
              "Symbol","id",
              "Symbol","value",
              "ASTNode","childIndex",
              "ASTNode","numChildren",
              "ASTNode","DUMP_TREE_INDENT",
              "ASTNode","parent",
              "ASTNode","children"
      };
    }
  }

  @Override
  public void analyzeNode(Object node, ASTClass astClass) {
      astrepresentationsize += NodeASTRepresentationSize.nodeAstRepresentationSize(node);
  }
  
  @Override
  protected void output(PrintWriter pw) {
    MemAnalysis.csvoutput(pw,
            astrepresentationsize );
  }

}
