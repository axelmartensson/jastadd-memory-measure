package measure;

import drast.model.ASTAnnotation;
import org.openjdk.jol.vm.VM;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by axel on 3/5/17.
 */
public class CacheRootSize extends MemAnalysis {

  private static class Result {
    String rootnode;
    long numentries;
    long hashmapoverhead;
    long keyoverhead;
    long keysmapoverhead;
    long totaloverhead;
  }
  List<Result> results;

  public CacheRootSize() throws FileNotFoundException {
    super("cacheroot.csv", "rootnode numentries hashmapoverhead keyoverhead keysmapoverhead totaloverhead testcase");
    results = new ArrayList<>();
  }

  @Override
  public void analyzeNode(Object node, ASTClass astClass) {
        Field f = findField(node, "CACHE");
        if (f != null) {
          Object cache = null;
          try {
            cache = f.get(node);
          } catch (IllegalAccessException e) {
            e.printStackTrace();
          }
          if (cache != null) {
            try {
              Map map = (Map) getAccessibleField("map", cache.getClass()).get(cache);
              Map keysmap = (Map) getAccessibleField("keys", cache.getClass()).get(cache);
              Result o = new Result();
              o.rootnode = cache.getClass().getSimpleName();
              o.numentries = map.size();
              o.hashmapoverhead = analyzeHashMap(map, map.getClass());
              o.keyoverhead = o.numentries*24;
              o.keysmapoverhead = analyzeKeysMap(keysmap);
              o.totaloverhead = o.hashmapoverhead + o.keyoverhead + o.keysmapoverhead;
              results.add(o);
            } catch (NoSuchFieldException | IllegalAccessException e) {
              throw new RuntimeException(e);
            }
          }
      }
  }

  @Override
  protected void output(PrintWriter pw) {
    for(Result r: results)
    csvoutput(pw, r.rootnode, r.numentries, r.hashmapoverhead, r.keyoverhead, r.keysmapoverhead, r.totaloverhead);
  }
}
