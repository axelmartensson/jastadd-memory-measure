package measure;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by axel on 3/5/17.
 */
public class ClassCount extends MemAnalysis {

  private Map<String, Long> seen;
  private PrintWriter structureOutput;
  static Long one = (long) 1;

  public ClassCount() throws FileNotFoundException {
    super("classcount.csv", "class count testcase");
    seen = new HashMap<>();
  }

  @Override
  public void analyzeNode(Object node, ASTClass astClass) {
    String name = astClass.getName();
    Long l = seen.get(name);
    if (l == null) {
      l = (long)1;
    } else {
      l = l + 1;
    }
    seen.put(name, l);
  }
  @Override
  protected void output(PrintWriter pw) {
    for(Map.Entry e : seen.entrySet())
    MemAnalysis.csvoutput(pw, e.getKey(), e.getValue());
  }

}
