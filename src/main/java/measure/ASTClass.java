package measure;

import org.openjdk.jol.vm.VM;

import drast.model.ASTAnnotation;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

/** immutable Value-object for shared properties across ast nodes
 * do most of the "static" reflection while building this object
 * if this part is found to take time in the profiler, it could even be serialized and indexed on the testcase
 */
public class ASTClass {
    String name;
    long size;
    Map<String, List<Method>> ntas;
    List<Method> tokens;
    List<Method> parameterized;
  List<Method> unparameterized;

  public ASTClass(String name, Object treeNode) {
      this.name = name;
      this.size = VM.current().sizeOf(treeNode);
      addNTAs(treeNode);
      addTokens(treeNode);
      addAttributes(treeNode);
    }
    private void addNTAs(Object treeNode) {
      ntas = new LinkedHashMap<>();
      //TODO map is now for caching at different levels, associating ntas with class names
      Class<?> klass = treeNode.getClass();
      while(!klass.getSimpleName().equals("Object")) {
        List<Method> methods = Arrays.stream(klass.getDeclaredMethods())
                .filter(MemAnalysis::isNTA)
                .sorted(Comparator.comparing(Method::getName))
                .collect(Collectors.toList());
        ntas.put(klass.getSimpleName(), methods);
        klass = klass.getSuperclass();
      }
    }

    private void addAttributes(Object treeNode) {
      Map<Boolean,List<Method>> part = Arrays.stream(treeNode.getClass().getMethods())
              .filter(MemAnalysis::isAttribute)
              .collect(Collectors.partitioningBy(ASTAnnotation::isParameterized));
      parameterized = part.get(true);
      unparameterized = part.get(false);

    }

    private void addTokens(Object treeNode) {
      tokens = Arrays.stream(treeNode.getClass().getMethods())
              .filter(MemAnalysis::isToken)
              .sorted(Comparator.comparing(t -> t.getName().substring(3)))
              .collect(Collectors.toList());
    }

    public Map<String,List<Method>> getNtas() {
      return ntas;
    }

    public String getAllTokensAsString(Object treeNode){
      return tokens.stream()
              .map(t -> " "+ t.getName().substring(3) + "=\""+MemAnalysis.invoke(treeNode, t)+"\"")
              .collect(Collectors.joining());
    }

    public long getSize() {
      return size;
    }

    public List<Method> getParameterized() {
      return parameterized;
    }

    public List<Method> getUnparameterized() {
      return unparameterized;
    }

    public String getName() {
      return name;
    }

}
