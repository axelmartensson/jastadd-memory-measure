package measure;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by axel on 3/5/17.
 */
public class EmptyCount extends MemAnalysis {
  private long emptylistcount;
  private long emptyoptcount;

  private Set<String> seen;
  private PrintWriter structureOutput;

  public EmptyCount() throws FileNotFoundException {
    super("empty.csv", "emptylist emptyopt testcase");
    seen = new HashSet<>(4);
    structureOutput = getPrintWriter("internalstructure.out","");
    structureOutput.println(System.getProperty("testcase", ""));
  }

  @Override
  public void analyzeNode(Object node, ASTClass astClass) {
    String name = astClass.getName();
    if (name.endsWith("List")&& MemAnalysis.numNonNullInChildArray(node) == 0) {
      emptylistcount++;
      if(seen.add(name)) printInternalStructure(node, structureOutput);
    } else if (name.endsWith("Opt") && MemAnalysis.numNonNullInChildArray(node) == 0) {
      emptyoptcount++;
      if(seen.add(name)) printInternalStructure(node, structureOutput);
    }
  }
  @Override
  protected void output(PrintWriter pw) {
    MemAnalysis.csvoutput(pw,
            emptylistcount,
            emptyoptcount);
  }

}
