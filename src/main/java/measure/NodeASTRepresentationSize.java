package measure;

import org.openjdk.jol.vm.VM;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Created by axel on 3/5/17.
 */
public class NodeASTRepresentationSize extends MemAnalysis {

  public NodeASTRepresentationSize() throws FileNotFoundException {
    super("nodeastrepresentationsize.csv", "node nodeastrepresentationsize testcase");
  }

  @Override
  public void analyzeNode(Object node, ASTClass astClass) {
    MemAnalysis.csvoutput(pw, astClass.getName(), nodeAstRepresentationSize(node));
  }

  public static long nodeAstRepresentationSize(Object node) {
    try {
      long nodeAstRepSize = VM.current().sizeOf(node); // actual node size
      Object children = findField(node, "children").get(node);
      nodeAstRepSize += children==null?0:VM.current().sizeOf(children); // actual children array size
      return nodeAstRepSize;
    } catch (IllegalAccessException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }


  @Override
  protected void output(PrintWriter pw) {
  }

}
