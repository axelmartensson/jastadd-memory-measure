package measure;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import static measure.MemAnalysis.csvoutput;
import static measure.MemAnalysis.getPrintWriter;

/**
 * Created by axel on 3/30/17.
 */
public class TimeAnalysis {
  private static long starttime;
  public static void start() {
    if(System.getProperty("timeanalysis", "").equals("true")) {
      starttime = System.nanoTime();
    }
  }
  public static void stop() {
    if (System.getProperty("timeanalysis", "").equals("true")) {
      long stoptime = System.nanoTime();
      try (PrintWriter time = getPrintWriter("time.csv", "time testcase")) {
        csvoutput(time, stoptime - starttime);

      } catch (FileNotFoundException e) {
        e.printStackTrace();
      } finally {
        System.exit(0);
      }
    }
  }
}
