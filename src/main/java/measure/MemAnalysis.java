package measure;

import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import org.openjdk.jol.info.FieldLayout;
import org.openjdk.jol.vm.VM;

import drast.model.ASTAnnotation;

    public abstract class MemAnalysis implements Closeable {
        private static final String IND = "  ";
        static Object globalCache;
        static HashMap<String, ASTClass> nodes;
        static long totalMemory;

      protected PrintWriter pw;

      public MemAnalysis(String name, String header) throws FileNotFoundException {
        pw = MemAnalysis.getPrintWriter(name, header);
      }

      public abstract void analyzeNode(Object node, ASTClass astClass);
      protected abstract void output(PrintWriter pw);

      @Override
      public void close() throws IOException {
        output(pw);
        pw.close();
      }


      public static void objectSize(Object root) {
            if(System.getProperty("memoryanalysis", "").equals("true")) {
                List<MemAnalysis> analyses = new LinkedList<>();

                try(
                        //TODO: remove unused csvfiles
                        PrintWriter totalmem = getPrintWriter("totalmemory.csv", "totalmemory testcase");
                        PrintWriter output = getPrintWriter("size.csv", "name size testcase");
                        PrintWriter counts = getPrintWriter("count.csv", "name count leaf testcase");
                        PrintWriter outputAttrib = getPrintWriter("attrib.csv", "attribute fieldtype size node testcase");
                        PrintWriter tree = getPrintWriter("treeDump"+System.getProperty("testcase", ""), " vim: shiftwidth=2 foldmethod=indent");
                        //PrintWriter gc = getPrintWriter("globalcache.csv", "size testcase");
                        //MemAnalysis paramCacheSize = new ParamCacheSize();
                        //MemAnalysis unparamCacheSize = new UnparamCacheSize();
                        //MemAnalysis cacheRootSize = new CacheRootSize();
                        //MemAnalysis coeffSize = new CoeffSize();
                        //MemAnalysis emptyCount = new EmptyCount();
                        //MemAnalysis vNodeRatio = new VNodeRatio();
                    ){

                  if(System.getProperty("totalmemory", "").equals("true")) {
                    totalMemory = totalMemory();
                    csvoutput(totalmem, totalMemory);
                  }
                  if(System.getProperty("underuse", "").equals("true")) {
                    analyses.add(new UnderUse());
                  }
                  if(System.getProperty("overhead", "").equals("true")) {
                    analyses.add(new Overhead());
                  }
                  if(System.getProperty("paramcacheelementcount", "").equals("true")) {
                    analyses.add(new ParamCacheElementCount());
                  }
                  if(System.getProperty("emptycount", "").equals("true")) {
                    analyses.add(new EmptyCount());
                  }
                  if(System.getProperty("classcount", "").equals("true")) {
                    analyses.add(new ClassCount());
                  }
                  if(System.getProperty("childrenarraysize", "").equals("true")) {
                    analyses.add(new ChildrenArraySize());
                  }
                  if(System.getProperty("inferastnodefields", "").equals("true")) {
                    analyses.add(new InferASTNodeFields());
                  }
                  if(System.getProperty("astrepresentationsize", "").equals("true")) {
                    analyses.add(new ASTRepresentationSize());
                  }

                  if(System.getProperty("nodeastrepresentationsize", "").equals("true")) {
                    analyses.add(new NodeASTRepresentationSize());
                  }

                  //FIXME: assume we can have more than one cache on nodes other than root node in traversal
                      initGlobalCache(root);
//                      if (globalCache != null) {
//                       csvoutput(gc, analyzeGlobalCache() / (float) totalMemory);
//                      }
                    typeSize(root, output, counts, outputAttrib, tree, analyses);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                  for (MemAnalysis m: analyses)
                    try {
                      m.close();
                    } catch (IOException e) {
                      e.printStackTrace();
                    }
                }
              System.exit(0);
            } else {
                //System.err.println("no memory analysis");
            }
        }

        static PrintWriter getPrintWriter(String fn, String header) throws FileNotFoundException {
            String csvdir = System.getProperty("csvdir", "");
            File file = new File(csvdir, fn);
            PrintWriter output = new PrintWriter(new FileOutputStream(file, true), true);
            System.out.println("ANALYSIS Writing results to: "+file.getAbsolutePath());
            if (file.length() <= 0) output.println(header);
            return output;

        }

        private static class Counter {
            HashMap<String, Long> count = new HashMap<String, Long>();

            public void increment(String name) {
                if(count.containsKey(name)){
                    count.put(name, count.get(name)+1);
                } else {
                    count.put(name, (long) 1);
                }

            }
            public void output(PrintWriter out, boolean leaf) {
                for(Map.Entry<String, Long> e:count.entrySet()){
                    csvoutput(out, e.getKey(), e.getValue(),leaf?"true":"false");
                }
            }
        }
        // assuming testcase is last column
        public static void csvoutput(PrintWriter pw, Object... items){
            String testcase = System.getProperty("testcase", "");
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < items.length; i++) {
                if(i!=0) sb.append(" ");
                sb.append(items[i].toString());
            }
            sb.append(" ");
            sb.append(testcase);
            pw.println(sb);

        }

        private static class TS {
            Object treeNode;
            String indent;

            public TS(Object treeNode, String indent) {
                this.treeNode = treeNode;
                this.indent = indent;
            }
        }
        private static class NTATS extends TS {
            String attributeName;
            String klassName;
            Object parameters;

            public NTATS(Object treeNode, String klassName, String attributeName, Object parameters, String indent) {
                super(treeNode, indent);
                this.attributeName = attributeName;
                this.klassName = klassName;
                this.parameters = parameters;
            }
        }


      public static void typeSize(Object root, PrintWriter sizes, PrintWriter countOut, PrintWriter attributes, PrintWriter tree, List<MemAnalysis> analyses) {

        nodes = new HashMap<String, ASTClass>();
        Set<Object> visited = new HashSet<Object>(); // assures monotonicity/termination(computed ntas are cyclic?)
        LinkedList<TS> stack = new LinkedList<>();
        stack.add(new TS(root, ""));
        while (!stack.isEmpty()) {
          TS ts = stack.pollLast();
          if (!visited.contains(ts.treeNode)) {
            String name = ts.treeNode.getClass().getSimpleName();
            ASTClass node = nodes.computeIfAbsent(name, n -> {
              //analyzeClass(sizes, nodes, ts.treeNode, n);
              return new ASTClass(n, ts.treeNode);
            });
            for (MemAnalysis a: analyses) a.analyzeNode(ts.treeNode, node);
            //treeDump(tree, ts, name, node);

            addAllComputedNTAsInReverse(node, stack, ts);
            addAllChildrenInReverse(stack, ts.treeNode, ts.indent);
            visited.add(ts.treeNode);
          }
        }
      }

      private static void initGlobalCache(Object root) {
        try {
          Field f = findField(root, "CACHE");
          if (f != null) globalCache = f.get(root);
        } catch (IllegalAccessException e) {
          System.out.println(e);
          e.printStackTrace();
        }
      }

      private static long analyzeString(Object str) throws NoSuchFieldException, IllegalAccessException {

        return VM.current().sizeOf(str) + VM.current().sizeOf(getAccessibleField("value", str.getClass()).get(str));
      }
      private static void treeDump(PrintWriter tree, TS ts, String name, ASTClass node) {
        //TODO test for dumpFormat=extendj or dumpFormat=analysis
        if(ts instanceof NTATS) {
            NTATS ntats = (NTATS) ts;
            tree.println(ntats.indent+ntats.klassName+"."+ntats.attributeName +"("+ (ntats.parameters==null?"":ntats.parameters)+")");
            tree.println(ntats.indent+IND+name+node.getAllTokensAsString(ntats.treeNode));
            ntats.indent += IND;
        } else {
            tree.println(ts.indent+name+node.getAllTokensAsString(ts.treeNode));
        }
      }

      /**
       * see ModelicaMiddleEnd/src/jastadd/Profiling.jrag
       */
      private static long totalMemory() {
        Runtime rt = Runtime.getRuntime();
        for (int i = 0; i < 500; i++) {
          rt.runFinalization();
          rt.gc();
        }
        return rt.totalMemory() - rt.freeMemory();
      }

      private static void analyzeClass(PrintWriter sizes, Map<String, ASTClass> nodes, Object treeNode, String name) {


//                        ClassLayout classLayout = ClassLayout.parseClass(treeNode.getClass());
//                        for (FieldLayout fieldLayout : classLayout.fields()) {
//                            if (
//                                    belongsToAttribute(fieldLayout)) {

//                                //skip last _part to get attribute name
//                                StringBuilder attributeName = new StringBuilder();
//                                String[] underscores = fieldLayout.name().split("_");
//                                for (int i = 0; i < underscores.length - 1; i++)
//                                    attributeName.append((i == 0 ? "" : "_") + underscores[i]);
//                                String fieldType = underscores[underscores.length - 1];
//                                csvoutput(attributes, attributeName, fieldType, fieldLayout.size(), name);
//                            }
//                        }
      }

      private static void addAllComputedNTAsInReverse(ASTClass node, LinkedList<TS> stack, TS ts) {
        List<NTATS> res = new LinkedList<>();
        for (Map.Entry<String, List<Method>> ac : node.getNtas().entrySet()) {
          String klassname = ac.getKey();
          List<Method> methods = ac.getValue();
          for (Method method: methods) {
            Object cachedValue = getComputed(ts.treeNode, klassname, method);
            if (cachedValue != null) {
              if (ASTAnnotation.isParameterized(method)) {
                  Map cache = (Map) cachedValue;
                  for (Object e : cache.entrySet()) {
                    Object key = ((Map.Entry) e).getKey();
                    Object params = null;
                    //TODO: check if it is globally cached first or catch the NoSuchFieldException and look for local cache after that
                    if (globalCache != null) { // assuming that we central cache all parameterized //FIXME: do not assume this
                      try {
                        params = getAccessibleField("parameters", key.getClass()).get(key);
                      } catch (IllegalAccessException e1) {
                        e1.printStackTrace();
                      } catch (NoSuchFieldException e1) {
                        e1.printStackTrace();
                      }
                    } else {
                      params = key;
                    }
                    res.add(new NTATS(((Map.Entry) e).getValue(), klassname, ASTAnnotation.getSignature(method), params, ts.indent + IND));
                  }
              } else {
                res.add(new NTATS(cachedValue, klassname, ASTAnnotation.getSignature(method), null, ts.indent + IND));
              }
            }
          }
        }
        Collections.reverse(res);
        stack.addAll(res);
      }

        private static void addAllChildrenInReverse(LinkedList<TS> stack, Object treeNode, String indent) {
          try {
            Object children = findField(treeNode, "children").get(treeNode);//FIXME: getAccessibleField on ASTNode directly saves lookup
            if(children != null) {
              for (int i = 0; i < Array.getLength(children); i++) {
                Object n = Array.get(children, i);
                if (n != null) stack.add(new TS(n, indent + IND));
              }
            }
          } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
          }
        }


        private static void analyzePairMap(PrintWriter caches, String name, Method method, Map cache, Class cacheklass) throws NoSuchFieldException, IllegalAccessException {
            Object table1 = getAccessibleField("keys", cacheklass).get(cache);
            Object table2 = getAccessibleField("values", cacheklass).get(cache);
            long tablesizes = VM.current().sizeOf(table1)+VM.current().sizeOf(table2);
            csvoutput(caches,
                    method.getName(),
                    cache.size(),
                    VM.current().sizeOf(cache),
                    tablesizes,
                    0,
                    name);
        }

        protected static long analyzeKeysMap(Object cache) throws NoSuchFieldException, IllegalAccessException {
          //variant of analyzeHashMap
          Object table = getAccessibleField("table", cache.getClass()).get(cache);
          long mapentrysize = 0;
          if (table != null) {
            for (int i = 0; i < Array.getLength(table); i++) {
              Object root = Array.get(table, i);
              if (root != null) {
                LinkedList<Object> stack = new LinkedList<>();
                stack.add(root);
                while (!stack.isEmpty()) {
                  Object o = stack.pollLast();
                  mapentrysize += VM.current().sizeOf(o);
                  Object key = getAccessibleField("key", o.getClass()).get(o);
                  mapentrysize += VM.current().sizeOf(key);
                  Object value = getAccessibleField("value", o.getClass()).get(o);
                  mapentrysize += analyzeLinkedList(value);
                  stack.addAll(hashMapChildren(o));
                }
              }
            }
          }
          long tablesize = table==null?0:VM.current().sizeOf(table);
          return VM.current().sizeOf(cache) + tablesize + mapentrysize;
        }

      protected static long analyzeLinkedList(Object list) throws NoSuchFieldException, IllegalAccessException {
          Object n = getAccessibleField("first", list.getClass()).get(list);
          long listentrysize = 0;
          while (n != null) {
            listentrysize += VM.current().sizeOf(n);
            n = getAccessibleField("next", n.getClass()).get(n);
          }
          return VM.current().sizeOf(list) + listentrysize;
        }

        protected static long analyzeHashMap(Map cache, Class cacheklass) throws NoSuchFieldException, IllegalAccessException {
            Object table = getAccessibleField("table", cacheklass).get(cache);
            long mapentrysize = 0;
          if (table != null) {
            for (int i = 0; i< Array.getLength(table); i++) {
                Object root = Array.get(table, i);
              if (root != null) {
                LinkedList<Object> stack = new LinkedList<>();
                stack.add(root);
                while (!stack.isEmpty()) {
                  Object o = stack.pollLast();
                  mapentrysize += VM.current().sizeOf(o);
                  stack.addAll(hashMapChildren(o));
                  }
                }
              }
          }
          long tablesize = table==null?0:VM.current().sizeOf(table);
          return VM.current().sizeOf(cache) + tablesize + mapentrysize;
            }

      protected static Collection<?> hashMapChildren(Object o) throws NoSuchFieldException, IllegalAccessException {
        List l = new ArrayList(2);
        if(o.getClass().getSimpleName().equals("TreeNode")){
          addNN(l, getAccessibleField("right", o.getClass()).get(o));
          addNN(l, getAccessibleField("left", o.getClass()).get(o));
        } else if (o.getClass().getSimpleName().equals("Entry")){ // assuming LinkedHashMap.Entry
          addNN(l, getAccessibleField("next", o.getClass().getSuperclass()).get(o));
        }else if (o.getClass().getSimpleName().equals("Node")) {
          addNN(l, getAccessibleField("next", o.getClass()).get(o));

        } else {
          throw new RuntimeException("unknown entry type in hashmap: " + o.getClass().getSimpleName());
        }
        return l;
      }


      private static void analyzeBoundedMap(PrintWriter caches, String name, Method method, Map cache, Class cacheklass) throws NoSuchFieldException, IllegalAccessException {
            //TODO count linked list as well
            Field t = getAccessibleField("table", cacheklass.getSuperclass().getSuperclass());
            Object table = t.get(cache);
            Object mapentry = null;
            for (int i = 0; i< Array.getLength(table); i++) {
                Object e = Array.get(table, i);
                if (e != null) {
                    mapentry = e;
                }
            }
            if (mapentry == null) {
                throw new RuntimeException("mapentry is null: "+name+"."+method.getName());
            }
            csvoutput(caches,
                    method.getName(),
                    cache.size(),
                    VM.current().sizeOf(cache),
                    VM.current().sizeOf(table),
                    VM.current().sizeOf(mapentry),
                    name);
        }

      public static void addNN(Collection l, Object o) {
        if (o != null) l.add(o);
      }

        public static boolean isAttribute(Method x) {
            return Arrays.stream(x.getAnnotations())
                    .filter(ASTAnnotation::isAttribute)
                    .findAny()
                    .isPresent();
        }

        public static boolean isToken(Method x) {
            return Arrays.stream(x.getAnnotations())
                    .filter(ASTAnnotation::isToken)
                    .findAny()
                    .isPresent();
        }


        public static Object invoke(Object treeNode, Method t) {
            try {
                return t.invoke(treeNode);
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException("reflection: could not invoke  on "+treeNode.getClass().getName());
            }
        }

        protected static Object getCachedValue(Object treeNode, Object klassname, Method method) {
            try {
            Field f = findField(treeNode, ASTAnnotation.getCacheValueFieldName(method));
            if (f != null) {
                return f.get(treeNode);
            } else { //assuming in global cache
                return getGlobalCached(treeNode, klassname, method);
            }
        } catch (IllegalAccessException x) {
            System.out.println(x);
            x.printStackTrace();
        }
        throw new RuntimeException("reflection: could not invoke getCachedValue" );
        }

      protected static Object isComputed(Object treeNode, Object klassname, Method method) {
        try {
          if (ASTAnnotation.isParameterized(method)) { //FIXME do not parameterized also have computed flag?, handle computed hashmap
            return getCachedValue(treeNode, klassname, method);
          } else {
            //FIXME shouldn't this be getDeclaredField since cache blocks are declared on nodes?
            Field f = findField(treeNode, ASTAnnotation.getComputedFlagFieldName(method));
            if (f != null) {
              Object o = f.get(treeNode);
              if (o instanceof Boolean && ((Boolean) o) || !(o instanceof Boolean) && o != null) {
                return true;
              }
            }
            return false;
          }
        } catch (IllegalAccessException x) {
          System.out.println(x);
          x.printStackTrace();
        }
        throw new RuntimeException("reflection: could not invoke isComputed" );
      }

        protected static Object getComputed(Object treeNode, Object klassname, Method method) {
            try {
            if (ASTAnnotation.isParameterized(method)) { //FIXME do not parameterized also have computed flag?
                return getCachedValue(treeNode, klassname, method);
            } else {
                //FIXME shouldn't this be getDeclaredField since cache blocks are declared on nodes?
                Field f = findField(treeNode, ASTAnnotation.getComputedFlagFieldName(method));
                if (f != null) {
                    Object o = f.get(treeNode);
                    if (o instanceof Boolean && ((Boolean) o) || !(o instanceof Boolean) && o != null) {
                        return getCachedValue(treeNode, klassname, method);
                    }
                }
                return null;
            }
        } catch (IllegalAccessException x) {
            System.out.println(x);
            x.printStackTrace();
        }
        throw new RuntimeException("reflection: could not invoke isComputed" );
        }

      private static Object getGlobalCached(Object treeNode, Object klassname, Method method) {
            try {
              if (globalCache != null) {
                //TODO calculate the signature client-side instead of relying on the oo revealing annotation
                Object signature = ASTAnnotation.getSignature(method);
                if(ASTAnnotation.isParameterized(method)) {
                  return globalCache.getClass().getDeclaredMethod("getParameterized", Object.class, Object.class, Object.class)
                          .invoke(globalCache, treeNode, klassname, signature);
                } else {
                  return globalCache.getClass().getDeclaredMethod("get", Object.class, Object.class, Object.class)
                          .invoke(globalCache, treeNode, klassname, signature);
                }
            }
            } catch (IllegalAccessException e) {
                System.out.println(e);
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                System.out.println(e);
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                System.out.println(e);
                e.printStackTrace();
            }
            return null;
        }

        protected static Field findField(Object treeNode, String name) {
            Class<?> klass = treeNode.getClass();
            while(!klass.getSimpleName().equals("Object")) {
                try {
                    return getAccessibleField(name, klass);
                } catch (NoSuchFieldException e) {
                   // keep searching up the class hierarchy
                }
                klass = klass.getSuperclass();
            }
            return null;
        }

        protected static Field getAccessibleField(String name, Class<?> klass) throws NoSuchFieldException {
            Field field = klass.getDeclaredField(name);
            field.setAccessible(true);
            return field;
        }

        public static boolean isNTA(Method method) {
                for (Annotation a:
                        method
                        .getAnnotations()) {
                   if (ASTAnnotation.isAttribute(a)
                                && ASTAnnotation.is(a, ASTAnnotation.AST_METHOD_NTA))
                       return true;
                }
                return false;

        }

        private static Object getChild(Object treeNode, int i) {
            try {
                return getParent("ASTNode", treeNode).getDeclaredMethod("getChildNoTransform", int.class).invoke(treeNode, i);
            } catch (ClassNotFoundException x) {
                System.out.println(x);
                x.printStackTrace();
            } catch (InvocationTargetException x) {
                System.out.println(x);
                x.printStackTrace();
            } catch (NoSuchMethodException x) {
                System.out.println(x);
                x.printStackTrace();
            } catch (IllegalAccessException x) {
                System.out.println(x);
                x.printStackTrace();
            }
            throw new RuntimeException("reflection: could not invoke getChild on "+treeNode.getClass().getName());
        }

        private static Class getParent(String name, Object start) throws ClassNotFoundException {
            Class<?> klass = start.getClass();
            while(!klass.getSimpleName().equals("Object")) {
                if (klass.getSimpleName().equals(name)) return klass;
                klass = klass.getSuperclass();
            }
            throw new ClassNotFoundException(klass.getClass().getName()+" not a superclass of "+start.getClass().getName());
        }

        public static long numNonNullInChildArray(Object treeNode) {
              long count = 0;
              try {
                Object children = findField(treeNode, "children").get(treeNode);//FIXME: getAccessibleField on ASTNode directly saves lookup
                if(children != null) {
                  for (int i = 0; i < Array.getLength(children); i++) {
                    Object n = Array.get(children, i);
                    if (n != null) count++;
                  }
                }
              } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
              }
              return count;
        }

      static boolean belongsToAttribute(String fieldLayout) {
        return belongsToAttributeCache(fieldLayout) ||
                belongsToAttributeOther(fieldLayout);
      }
      static boolean belongsToAttributeCache(String fieldLayout) {
        return  fieldLayout.endsWith("_value") ||
                fieldLayout.endsWith("_computed") ||
                fieldLayout.endsWith("_values");
      }

        static boolean belongsToAttributeOther(String fieldLayout) {
            return  fieldLayout.endsWith("_visited") ||
                    fieldLayout.endsWith("_initialized") ||
                    fieldLayout.endsWith("_proxy") ||
                    fieldLayout.endsWith("_circle");
        }

      static void printInternalStructure(Object node, PrintWriter debugout) {
        org.openjdk.jol.info.ClassLayout classLayout = org.openjdk.jol.info.ClassLayout.parseClass(node.getClass());
        debugout.println(classLayout.toPrintable(node));
        debugout.println("--------------------------------\n");
      }
    }
