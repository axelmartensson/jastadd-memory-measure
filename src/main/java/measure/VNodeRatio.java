package measure;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by axel on 3/5/17.
 */
public class VNodeRatio extends MemAnalysis {

  private Map<Object, Long> nodecounts;
  private Map<KAKey, Long> valuecounts;

  private static  class KAKey {
    Object klass;
    Object attribute;

    public KAKey(Object klass, Object attributeKey) {
      this.klass = klass;
      this.attribute = attributeKey;
    }

    @Override
    public boolean equals(Object o) {
      if (!(o instanceof KAKey)) return false;
      KAKey key = (KAKey) o;
      return klass.equals(key.klass) && attribute.equals(key.attribute);
    }

    @Override
    /** see {@link List#hashCode} */
    public int hashCode() {
      return (31 + hc(attribute))*31 + hc(klass);
    }
    private int hc(Object o) {return o==null?0:o.hashCode();}

    @Override
    public String toString() {
      return klass.toString() + " " + attribute.toString();
    }
  }
  private static class Result {
    String node;
    String attribute;
    long nodecount;
    long valuecount;
    double vnoderatio;
  }
  List<Result> results;

  public VNodeRatio() throws FileNotFoundException {
    super("vnoderatio.csv", "node attribute nodecount valuecount vnoderatio testcase");
    results = new ArrayList<>();
    nodecounts = new HashMap<>();
    valuecounts = new HashMap<>();
  }

  @Override
  public void analyzeNode(Object node, ASTClass astClass) {
      Class<?> nodeClass = node.getClass();
      while(!nodeClass.getSimpleName().equals("Object")) {
        nodecounts.compute(nodeClass.getSimpleName(), (k, v) -> v == null ? 1 : v + 1);
        nodeClass = nodeClass.getSuperclass();
      }

        Field f = findField(node, "CACHE");
        if (f != null) {
          Object cache = null;
          try {
            cache = f.get(node);
          } catch (IllegalAccessException e) {
            e.printStackTrace();
          }
          if (cache != null) {
            try {
              Map map = (Map) getAccessibleField("map", cache.getClass()).get(cache);
              //aggregate count grouped by klass and attribute
              for(Object cacheKey : map.keySet()) {
                String klass = (String) getAccessibleField("klass", cacheKey.getClass()).get(cacheKey);
                String attribute = (String) getAccessibleField("attribute", cacheKey.getClass()).get(cacheKey);
                valuecounts.compute(new KAKey(klass, attribute), (k, v) -> v==null?1:v+1);
              }
            } catch (NoSuchFieldException | IllegalAccessException e) {
              throw new RuntimeException(e);
            }
          }
      }
  }

  @Override
  protected void output(PrintWriter pw) {
    for(Map.Entry<KAKey, Long> e: valuecounts.entrySet()) {
      KAKey k = e.getKey();
      long valuecount = e.getValue();
      long nodecount = nodecounts.get(k.klass);
      Result r = new Result();
      r.node = (String) k.klass;
      r.attribute = (String) k.attribute;
      r.nodecount = nodecount;
      r.valuecount = valuecount;
      r.vnoderatio = valuecount / (double) nodecount;
      results.add(r);
    }
    for(Result r: results)
    csvoutput(pw, r.node, r.attribute, r.nodecount, r.valuecount, r.vnoderatio);
  }

}
