package measure;

import drast.model.ASTAnnotation;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by axel on 3/5/17.
 */
public class ParamCacheSize extends MemAnalysis {
  private long paramcachesize;
  private long numentries;
  private long numnull;

  public ParamCacheSize() throws FileNotFoundException {
    super("paramcache.csv", "paramcache numentries numnull testcase");
  }

  @Override
  public void analyzeNode(Object node, ASTClass astClass) {
      for (Method method : astClass.getParameterized()) {
        Field f = findField(node, ASTAnnotation.getCacheValueFieldName(method));
        if (f != null) {
          paramcachesize += 8;
          Object cachedValue = null;
          try {
            cachedValue = f.get(node);
          } catch (IllegalAccessException e) {
            e.printStackTrace();
          }
          if (cachedValue == null) {
            numnull++;
          } else {
            try {
              Map cache = (Map) cachedValue;
              numentries += cache.size();
              Class cacheklass = cache.getClass();
              if (cacheklass.getSimpleName().equals("HashMap")) {
                paramcachesize += analyzeHashMap(cache, cacheklass);
              } else if (cacheklass.getSimpleName().equals("LinkedHashMap")) {
                paramcachesize += analyzeHashMap(cache, cacheklass.getSuperclass());
              } else if (cacheklass.getSimpleName().equals("BoundedMap")) {
                paramcachesize += analyzeHashMap(cache, cacheklass.getSuperclass().getSuperclass());
                //return analyzeBoundedMap(caches, name, method, cache, cacheklass);
                //System.out.println(astClass.getName()+" "+method.getName()+" "+cache.size());
//              } else if (cacheklass.getSimpleName().equals("PairMap")) {
//                return analyzePairMap(caches, name, method, cache, cacheklass);
              } else {
                throw new RuntimeException("Unknown cache type: " + cacheklass.getSimpleName());
              }
            } catch (NoSuchFieldException | IllegalAccessException e) {
              throw new RuntimeException(e);
            }
          }
        }
      }
  }

  @Override
  protected void output(PrintWriter pw) {
    csvoutput(pw, paramcachesize, numentries, numnull);
  }
}
