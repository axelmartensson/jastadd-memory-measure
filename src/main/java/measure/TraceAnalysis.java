package measure;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import static measure.MemAnalysis.csvoutput;
import static measure.MemAnalysis.getPrintWriter;

/**
 * Created by axel on 4/17/17.
 */
public class TraceAnalysis {
  private PrintWriter file;

  public TraceAnalysis() {
    try {
      file = getPrintWriter("trace.csv", "node declClass attribute testcase");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  //mirrors the methods in Tracer.tt more or less, delegated to from the hook
  public void cacheEviction(Object node, Object attributeStr, Object params, Object value) {
    //String[] parts = ((String) attributeStr).split(".");
    //String declClass = parts[0];
    //String attribute = parts[1];
    csvoutput(file, node.getClass().getSimpleName(), attributeStr);
  }
}
