package measure;

import drast.model.ASTAnnotation;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * Created by axel on 3/5/17.
 */
public class ParamCacheElementCount extends MemAnalysis {
  private static class Result {
    String node;
    String attribute;
    long entrycount;
  }
  List<Result> results;

  public ParamCacheElementCount() throws FileNotFoundException {
    super("paramcacheelementcount.csv", "node attribute elementcount size testcase");
  }

  @Override
  public void analyzeNode(Object node, ASTClass astClass) {
      for (Method method : astClass.getParameterized()) {
        Field f = findField(node, ASTAnnotation.getCacheValueFieldName(method));
        if (f != null) {
          Object cachedValue = null;
          try {
            cachedValue = f.get(node);
          } catch (IllegalAccessException e) {
            e.printStackTrace();
          }
          if (cachedValue == null) {
          } else {
              Map cache = (Map) cachedValue;
            try {
              csvoutput(pw, astClass.getName(), ASTAnnotation.getSignature(method),
                      cache.size(), analyzeHashMap(cache, cache.getClass()));
            } catch (NoSuchFieldException e) {
              e.printStackTrace();
            } catch (IllegalAccessException e) {
              e.printStackTrace();
            }
          }
        }
      }
  }

  @Override
  protected void output(PrintWriter pw) {
  }
}
