package measure;

import drast.model.ASTAnnotation;
import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.info.FieldLayout;
import org.openjdk.jol.vm.VM;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Created by axel on 3/5/17.
 */
public class ChildrenArraySize extends MemAnalysis {
  private long childrenarraysize;

  private final int CUTOFF = 50;

  public ChildrenArraySize() throws FileNotFoundException {
    super("childrenarraysize.csv", "childrenarraysize testcase");
  }

  @Override
  public void analyzeNode(Object node, ASTClass astClass) {

    try {
     Object children = findField(node, "children").get(node);
     childrenarraysize += children==null?0:VM.current().sizeOf(children); // actual children array size

    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  private long fieldCost(ClassLayout classLayout, String name) throws NoSuchFieldException{
    for (FieldLayout fieldLayout :classLayout.fields()) {
      if(fieldLayout.hostClass().equals("ASTNode") && fieldLayout.name().equals(name)) return fieldLayout.size();
    }
    throw new NoSuchFieldException("Field " + name + " not found in a node");
  }
  @Override
  protected void output(PrintWriter pw) {
    MemAnalysis.csvoutput(pw, childrenarraysize);
  }

}
