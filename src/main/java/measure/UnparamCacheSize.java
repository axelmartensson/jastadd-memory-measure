package measure;

import org.openjdk.jol.datamodel.X86_64_DataModel;

import drast.model.ASTAnnotation;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by axel on 3/5/17.
 */
public class UnparamCacheSize extends MemAnalysis {
  private long unparamcachesize;
  private long numentries;
  private long numnull;
  private X86_64_DataModel dm;

  public UnparamCacheSize() throws FileNotFoundException {
    super("unparamcache.csv", "unparamcache numentries numnull testcase");
    this.dm = new X86_64_DataModel();
  }

  @Override
  public void analyzeNode(Object node, ASTClass astClass) {
      for (Method method : astClass.getUnparameterized()) {
        Field f = findField(node, ASTAnnotation.getCacheValueFieldName(method));
        if (f != null) {
          try {
            Object o = f.get(node);
            if (o == null) { // it is a reference
              unparamcachesize += 8;
              numnull++;
            } else {
              unparamcachesize += dm.sizeOf(o.getClass().getSimpleName());
              numentries++;
            }
          } catch (IllegalAccessException e) {
            e.printStackTrace();
          }
        }
      }
  }

  @Override
  protected void output(PrintWriter pw) {
    csvoutput(pw, unparamcachesize, numentries, numnull);
  }
}
