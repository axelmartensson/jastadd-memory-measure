package measure;

import org.openjdk.jol.info.FieldLayout;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by axel on 3/5/17.
 */
public class InferASTNodeFields extends MemAnalysis {
  private Set<String> seen;
  private Set<MyField> fields;
  private PrintWriter structureOutput;
  private Object bottom;

  public InferASTNodeFields() throws FileNotFoundException {
    super("astnodefields.csv", "typeClass hostClass name size testcase");
    seen = new HashSet<>(4);
  }

  @Override
  public void analyzeNode(Object node, ASTClass astClass) {
    if (fields == null) { // Init to bottom value (root node)
      fields = new HashSet<>();
      fields.addAll(collectFields(node));
      return;
    }

    String name = astClass.getName();
    if(seen.add(name)) {
      fields.retainAll(collectFields(node));
    }
  }

  private Set<MyField> collectFields(Object node) {
    return org.openjdk.jol.info.ClassLayout.parseClass(node.getClass())
            .fields().stream()
            .map(fl -> new MyField(fl.typeClass(), fl.hostClass(), fl.name(), fl.size()))
            .collect(Collectors.toSet());
  }

  @Override
  protected void output(PrintWriter pw) {
    for (MyField f : fields) {
      MemAnalysis.csvoutput(pw, f.typeClass, f.hostClass, f.name, f.size);
    }
  }

  private class MyField {
    private final String typeClass;
    private final String hostClass;
    private final String name;
    private final long size;

    public MyField(String typeClass, String hostClass, String name, long size) {
      this.typeClass = typeClass;
      this.hostClass = hostClass;
      this.name = name;
      this.size = size;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      MyField myField = (MyField) o;

      if (size != myField.size) return false;
      if (!typeClass.equals(myField.typeClass)) return false;
      if (!hostClass.equals(myField.hostClass)) return false;
      return name.equals(myField.name);
    }

    @Override
    public int hashCode() {
      int result = typeClass.hashCode();
      result = 31 * result + hostClass.hashCode();
      result = 31 * result + name.hashCode();
      result = 31 * result + (int) (size ^ (size >>> 32));
      return result;
    }
  }
}
