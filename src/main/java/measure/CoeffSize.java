package measure;

import org.openjdk.jol.vm.VM;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.*;

/**
 * Created by axel on 3/5/17.
 */
public class CoeffSize extends MemAnalysis {

  private static class Result {
    long mapentrysize;
    long keysize;
    long coeffsize;
  }
  List<Result> results;

  public CoeffSize() throws FileNotFoundException {
    super("coeffsize.csv", "mapentrysize keysize coeffsize testcase");
    results = new ArrayList<>();
  }

  @Override
  public void analyzeNode(Object node, ASTClass astClass) {
        Field f = findField(node, "CACHE");
        if (f != null) {
          Object cache = null;
          try {
            cache = f.get(node);
          } catch (IllegalAccessException e) {
            e.printStackTrace();
          }
          if (cache != null) {
            try {
              Map map = (Map) getAccessibleField("map", cache.getClass()).get(cache);
              Object table = getAccessibleField("table", map.getClass()).get(map);
              Set<Long> mapentrysizes = new HashSet<>();
              Set<Long> keysizes = new HashSet<>();
              if (table != null) {
                for (int i = 0; i< Array.getLength(table); i++) {
                  Object root = Array.get(table, i);
                  if (root != null) {
                    LinkedList<Object> stack = new LinkedList<>();
                    stack.add(root);
                    while (!stack.isEmpty()) {
                      Object o = stack.pollLast();
                      mapentrysizes.add(VM.current().sizeOf(o));
                      Object key = getAccessibleField("key", o.getClass()).get(o);
                      keysizes.add(VM.current().sizeOf(key));
                      stack.addAll(hashMapChildren(o));
                    }
                  }
                }
              }
              for(Long mapentrysize : mapentrysizes) {
                for(Long keysize : keysizes) {
                  Result o = new Result();
                  o.mapentrysize = mapentrysize;
                  o.keysize = keysize;
                  o.coeffsize = keysize + mapentrysize + 1;// where 1 is the assumed per-entry cost of the table-array
                  results.add(o);
                }
              }
            } catch (NoSuchFieldException | IllegalAccessException e) {
              throw new RuntimeException(e);
            }
          }
      }
  }

  @Override
  protected void output(PrintWriter pw) {
    for(Result r: results)
    csvoutput(pw, r.mapentrysize, r.keysize, r.coeffsize);
  }
}
