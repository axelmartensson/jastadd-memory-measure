package measure;

import org.openjdk.jol.datamodel.X86_64_DataModel;
import org.openjdk.jol.vm.VM;

import drast.model.ASTAnnotation;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by axel on 3/5/17.
 */
public class UnderUse extends MemAnalysis {
  private long emptylistoptsize;
  private long unusedcachefieldsize;
  private X86_64_DataModel dm;

  private final int CUTOFF = 50;

  public UnderUse() throws FileNotFoundException {
    super("underuse.csv", "emptylistoptsize unusedcachefieldsize testcase");
    this.dm = new X86_64_DataModel();
  }

  @Override
  public void analyzeNode(Object node, ASTClass astClass) {
    String name = astClass.getName();
    if ((name.endsWith("List")||name.endsWith("Opt"))&& MemAnalysis.numNonNullInChildArray(node) == 0) {
      emptylistoptsize += VM.current().sizeOf(node);;
    }

    // Unparameterized caches
    for (Method method : astClass.getUnparameterized()) {
      Field f = findField(node, ASTAnnotation.getCacheValueFieldName(method));
      if (f != null) { // field is found => attribute is cached
          if(!(Boolean)MemAnalysis.isComputed(node, node.getClass(), method)) {
            unusedcachefieldsize += dm.sizeOf(findField(node, ASTAnnotation.getComputedFlagFieldName(method)).getType().getSimpleName()); // add computed flag
            unusedcachefieldsize += dm.sizeOf(f.getType().getSimpleName());
          }
      }
    }

    // Parameterized caches
    for (Method method : astClass.getParameterized()) {
      Field f = findField(node, ASTAnnotation.getCacheValueFieldName(method));
      if (f != null) {
        Object cachedValue = null;
        try {
          cachedValue = f.get(node);
        } catch (IllegalAccessException e) {
          e.printStackTrace();
        }
        if (cachedValue == null) {
          unusedcachefieldsize += 8;
          Field computed = findField(node, ASTAnnotation.getComputedFlagFieldName(method));
          if (computed != null) {
            unusedcachefieldsize += dm.sizeOf(computed.getType().getSimpleName()); // add computed flag
          }
        }
      }
    }

  }

  @Override
  protected void output(PrintWriter pw) {
    MemAnalysis.csvoutput(pw,
            emptylistoptsize,
            unusedcachefieldsize);
  }

}
