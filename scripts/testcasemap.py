import pandas as pd
_testcasenames = {
    "Spice3BenchmarkFourBitBinaryAdder": "JM1",
    "RoomCO2WithControls": "JM2",
    "ThreeTanks": "JM3",
    "antlr": "EXJ1",
    "clojure": "EXJ2",
    "jastaddj": "EXJ3",
    "javac": "EXJ4",
    "jdepend": "EXJ5",
    "jsilver": "EXJ6",
    "junit": "EXJ7",
    "jython": "EXJ8",
    "lucene": "EXJ9"
}


def maptestcasenames(df):
    return df.rename(lambda x: _testcasenames[x])

def addCompiler(df):
    def lbl(x):
        if x[0].isupper():
            return "jmodelica"
        else:
            return "extendj"
    df['compiler'] = [lbl(x) for x in df.index]

def funcCompileradd(df):
    newdf = pd.DataFrame(df)
    addCompiler(newdf)
    return newdf

def meanDescription(df):
    return df.loc[[(comp,stat) for comp in ['jmodelica', 'extendj'] for stat in ['mean']]].apply(lambda x: [round(f,0) for f in x]).T
