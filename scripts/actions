#!/bin/bash

# This is the collection of bash function sourced in the other driver scripts.
# This library contains bash functions representing actions that drive the run,
# a definition of the relationships between branches of the different projects,
# and bash functions for executing an action either for all branches or for a
# specific branch. 
# 
# Actions are represented by runnable bash functions. They are made for
# automating common build actions. There are high-level actions that are meant
# to be specified in the configuration file of a task directory and there are
# low-level actions that are used by the high-level actions. The two high-level
# actions are measureit and testit. They are both supported by the low-level
# actions buildit and runit.

# Tasks can have a task type. There are three task types: time, memory, and none. There are
# currently three task types: time, memory and none

buildit() {
# loads a file that can contain one jastadd option if it exists
if [ -f "jastaddoption" ]; then
    jastaddoption=`cat jastaddoption`
fi
cd $jastadd_root
git checkout $1
cd $extendj_root
git checkout $2
gradle clean jar ${jastaddoption+"-Pjastaddoptions=$jastaddoption"}


if [ $use_jmodelica = "yes" ]; then
patch=$jmodelica_patches_root/$3.patch
cd $jmodelica_root
if [ -f $patch ]; then
    patch -p0 < $patch
    echo "applied patch: $patch"
# TODO trap kill signal and cleanup patch
fi
cd Compiler/ModelicaCompiler
JMODELICA_HOME=$JMODELICA_HOME ant clean build ${4+"test"} ${jastaddoption+"-Djastaddoption=$jastaddoption"}
cd ../../
if [ -f $patch ]; then
    patch --reverse -p0 < $patch
    echo "reversed patch: $patch"
fi
fi
cd $pwd
}

runit() {
if [ $use_jmodelica = "yes" ]; then
$scriptdir/run-modelica
fi
$scriptdir/run-jjbench
}

# The task type specifiess how many iterations of the task is performed. 
# Parameters:
# - $1 task 
# - $2 
measureit() {
buildit $1 $2 $3
if [ $4 = "time" ] || [ $4 = "memory" ]; then
    for i in {1..32}
    do
        runit
    done
else
    runit
fi
mkdir -p $1
mv *.csv $1
if [ -d "transform_run" ]; then
    mv transform_run $1
fi
}

testit() {
buildit $1 $2 $3 test
mkdir -p $1
cd $pwd
if [ $use_jmodelica = "yes" ]; then
cp -r $jmodelica_root/Compiler/ModelicaCompiler/doc/junit-reports $1/jmodelica
#junit-reports/overview-summary.html
fi

cd $jastadd_test_root
git checkout $2
gradle test
cd $pwd
cp -r $jastadd_test_root/build/reports $1/jastadd-test

cd $extendj_regression_tests_root
git checkout $extendj_regression_tests_branch
gradle test
cd $pwd
cp -r $extendj_regression_tests_root/build/reports $1/extendj-regression-tests

}

# forall branches specified in the branches hash table, run the task $1 of task type $2.
forall-branches() {
for b in "${!branches[@]}"
do
    run-task-branch-tasktype ${1?} $b ${2?}
done
}

# run the task $1 of task type $3 on branch $2
run-task-branch-tasktype() {
${1?} ${2?} ${branches[$2]} ${3?}
}

# example invocations:
# run-task-branch-tasktype testit pairmap-bounded none
# forall-branches testit none
