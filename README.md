JastAdd Memory Measure
======================

This project benchmarks the JastAdd2 code generator using the two compilers: 
[ExtendJ](extendj.org) and [Jmodelica.org](jmodelica.org).

**Contents:**
[TOC]

Quick Start
-----------

All dependencies, see [External Dependencies](#external-dependencies), need to 
be satisfied for running all benchmarks. There is a script for setting up 
the project dependencies called `setup`. To clone the project and setup all dependencies, do the following:

    git clone git@bitbucket.org:axelmartensson/jastadd-memory-measure.git
    cd jastadd-memory-measure/scripts
    ./setup

This will clone all other repositories and set up symlinks, apply patches, etc.
By default the `setup`-script prompts the user for input. This can be disabled by
specifying the `-q`-flag at the prompt. The script will then clone all other
repositories in the same folder as this repository. The setup-script checks if the
files already exist, so it can be run multiple times if need be (it is kind-of idempotent).

The tasks are run by invoking the `run` script. The tasks are run for all 
branches unless a specific branch of the JastAdd2 project is specified using 
the `-b` flag:

0. `$ cd scripts`
1. Run tasks from the command line by specifying their name to the 
`run`-script, several tasks can be run sequentially:
    - run the time task:
        `$ ./run time`
    - or, run the time and totalmemory tasks on the master branch:
        `$ ./run -b master time totalmemory`
2. Add a new task:
    - a task can be added by creating a new directory in the scripts folder 
      with the name of the task. Do not forget the configuration files, see the 
      subsection [Task Configuration](#task-configuration). 
3. Clean the measurement data:
    - `git clean -fd`
Overview
--------

This will provide an overview of the structure of the repository.
.
├── build.gradle -- the build file
├── lib -- external libraries
├── README.md
├── scripts

External Dependencies
---------------------

This project depends on a few related projects which exist in separate 
repositories:

- JastAdd: <https://bitbucket.org/jastadd/jastadd2>
- JastAdd Test: 
- ExtendJ: 
- ExtendJ Regression Tests: 
- JModelica.org: <https://svn.jmodelica.org>
- JModelica patches:

The repositories that are linked for JastAdd and ExtendJ are not the official 
repositories of the projects, available under <https://bitbucket.org/jastadd/>, 
but rather forks of them that have been made to try out new methods of memory 
optimization. These forks come with a few extra branches which contain the 
various implementations. The branches and how they fit together is explained 
under the section [Configuration](#configuration). <!-- There is a script that 
can be used to automatically set up these forks, see -->

Running the tasks
-----------------

This will describe how the `run`-file works. Tasks can be evaluated in two 
ways: either by default, where the configuration files, see [Task 
Configuration] are used the top-level run-loop by 

Configuration
-------------

<!-- Configuration of paths is done using shell script variables in the 
`environment`-file which specify the paths for the external projects. -->
Tasks
-----

Each task resides in a *task directory*. The name of this directory is the name 
of the task.

### Task Configuration

The task directory contains a number of configuration files which are used by 
the driver scripts:

- `task`-which action to be performed by the task.
- `tasktype` - the type of the task.
- `test.properties`- options passed in to the reflection library.
- optional: `modelicaflags`
- optional:`jastaddoptions`.

Actions
-------

Two default actions are specified in `scripts/actions`, which can be used by the tasks:

- `measureit`
- `testit`

Why BASH?
---------

The driver scripts are written in BASH. This means that the control flow needs to be explicitly defined in the driver scripts.A more declarative approach would probably be easier to extend. Actions, see the above section, correspond to targets in a traditional Makefile. The system is "kind of" like make. The advantage and disadvantage with make is like always that everything is cast within that framework. Going over to make would let us get rid of the custom evaluator run-scripts.
Since the control flow is so simple, it is an advantage to have it explicitly in the run-scripts, however if it gets more complex, it might be warranted to switch to make.

declarative scripts only declare bash functions. All scripts which have `run` in the name are used to drive the  which do the actual evaluation the imperative iteration loops. Minimize the imperativeness, minimize the `run`-files, separation of concerns. We have tried to minimize the imperativeness by separating  these two. 

Each task directory can also specify a `run`-file, which can be executed instead of using the configuration file to decide on the action.

Troubleshooting
-----------

- `./configuration.conf: No such file or directory` => run `./setup- `, see Quick Start. The reason for this is that the configuration is generated from `scripts/configuration.template` when running setup. having a template and a config might not be ideal, but one of them is in the repository, the other is not. This separation prevents accidental commits of local configs.

